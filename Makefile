SHELL := /bin/bash

terraform: terraform-init source terraform-plan terraform-apply

terraform-init:
	@cd /home/user/newvapormap-ci/terraform; terraform init

source:
	@cd /home/user/newvapormap-ci/terraform; source /home/user/ms-icd_01-openrc.sh

terraform-plan:
	@cd /home/user/newvapormap-ci/terraform; terraform plan

terraform-apply:
	@cd /home/user/newvapormap-ci/terraform; terraform apply --auto-approve

terraform-output:
	@cd /home/user/newvapormap-ci/terraform; terraform output	

terraform-destroy:
	@cd /home/user/newvapormap-ci/terraform; terraform destroy --auto-approve

ansible-install:
	@cd /home/user/newvapormap-ci/ansible_files; ansible-playbook -i hosts install.yml 

ansible-deploy:
	@cd /home/user/newvapormap-ci/ansible_files; ansible-playbook -i hosts deploy-manifest.yml

ansible-delete:
	@cd /home/user/newvapormap-ci/ansible_files; ansible-playbook -i hosts delete.yml

ansible-test:
	@cd /home/user/newvapormap-ci/ansible_files; ansible-playbook -i hosts test.yml		

