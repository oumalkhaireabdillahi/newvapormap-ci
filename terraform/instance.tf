terraform {
    required_version = ">= 0.14.0"
    required_providers {
        openstack = {
            source = "terraform-provider-openstack/openstack"
            version = "~> 1.35.0"
        }
    }
}

provider openstack {
#    region = "regionOne"
}


## Network

data "openstack_networking_network_v2" "ext_network" {
  name = "external"
}


resource "openstack_networking_network_v2" "network_1" {
    name = "network_1"
}

resource "openstack_networking_subnet_v2" "subnet_1" {
    name = "subnet_1"
    network_id =  openstack_networking_network_v2.network_1.id
    cidr = "172.16.0.0/24"
    dns_nameservers = ["192.44.75.10", "192.108.115.2"]
    ip_version = 4
}
resource "openstack_networking_router_v2" "my_router" {
    name                = "my_router"
    external_network_id = data.openstack_networking_network_v2.ext_network.id
}


# Router interface configuration
resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.my_router.id
  subnet_id = openstack_networking_subnet_v2.subnet_1.id
}

# Create floating ip
resource "openstack_networking_floatingip_v2" "fip_http" {
  pool = data.openstack_networking_network_v2.ext_network.name
}





## security group

resource "openstack_networking_secgroup_v2" "secgroup_1" {
  name        = "secgroup_1"
  description = "My network security group"
}



resource "openstack_networking_secgroup_rule_v2" "rule-icmp-in" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "icmp"
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.secgroup_1.id
}

resource "openstack_networking_secgroup_rule_v2" "rule-ssh" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 22
    port_range_max    = 22
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.secgroup_1.id
}

resource "openstack_networking_secgroup_rule_v2" "rule-http" {
    direction         = "ingress"
    ethertype         = "IPv4"
    protocol          = "tcp"
    port_range_min    = 80
    port_range_max    = 80
    remote_ip_prefix  = "0.0.0.0/0"
    security_group_id = openstack_networking_secgroup_v2.secgroup_1.id
}


#### Instance

resource "openstack_compute_instance_v2" "master" {
  name              = "master"
  image_name        = "imta-ubuntu"
  flavor_name       = "s20.xlarge"
  key_pair          = var.key_pair
  security_groups   = ["default", "secgroup_1"]

  network {
      name = openstack_networking_network_v2.network_1.name
  }
}



# Attach floating ip to instance
resource "openstack_compute_floatingip_associate_v2" "http" {
  floating_ip = openstack_networking_floatingip_v2.fip_http.address
  instance_id = openstack_compute_instance_v2.master.id
}

output "ext_IP" {
    description = "IP address of floating IP"
    value = openstack_networking_floatingip_v2.fip_http.address
}

